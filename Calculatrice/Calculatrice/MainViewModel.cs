﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculatrice
{
    class MainViewModel : BaseNotifyPropertyChanged
    {

        public List<string> Historique {
            get { return GetValue<List<string>>(); }
            set { SetValue(value); }

        }
 

        public string Calcultext
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }

        public string num1
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }
        public float num2
        {
            get { return GetValue<float>(); }
            set { SetValue(value); }
        }
    }
}
