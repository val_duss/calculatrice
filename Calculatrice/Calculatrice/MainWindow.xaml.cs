﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;


namespace Calculatrice
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewModel mvm;
        private String currentOperator { get; set; }

        public MainWindow()
        {
            mvm = new MainViewModel();
            mvm.Calcultext = "";
            mvm.num1 = "";
            currentOperator = "";
            mvm.Historique = new List<string>();

            this.DataContext = mvm;
            InitializeComponent();

        }

        private void check()
        {
            if (mvm.Calcultext.Contains("Resultat = "))
            {
                //mvm.num1 = mvm.num1.Substring(11);
                mvm.num1 = "";
                mvm.Calcultext = "";
            }
            else if (mvm.Calcultext.Contains("Error!"))
            {
                //mvm.Calcultext = mvm.Calcultext.Substring(6);
                mvm.num1 = "";
                mvm.Calcultext = "";
            }
        }

        private void check2()
        {
            if (mvm.Calcultext.Contains("Resultat = "))
            {
                //mvm.num1 = mvm.num1.Substring(11);
                mvm.num1 = "";
                mvm.Calcultext = mvm.Calcultext.Substring(11, mvm.Calcultext.Length - 11);
            }
            else if (mvm.Calcultext.Contains("Error!"))
            {
                //mvm.Calcultext = mvm.Calcultext.Substring(6);
                mvm.num1 = "";
                mvm.Calcultext = "";
            }
        }

        public void addNumber(String newNumber)
        {
            check();
            int n = int.Parse(newNumber);
            if (n >= 0 && n <= 9)
            {
                check();
                mvm.Calcultext += newNumber;
                mvm.num1 += newNumber;
            }
        }

        public void addOperator(String newOperator)
        {
            check2();
            if (newOperator.Equals(" + ")
                || newOperator.Equals(" - ")
                || newOperator.Equals(" * ")
                || newOperator.Equals(" / ")
                || newOperator.Equals(" % ")
                || newOperator.Equals(" | ")
                || newOperator.Equals(" ^ "))
            {
 
                currentOperator = newOperator;
                mvm.Calcultext += currentOperator;

                mvm.num1 = "";
            }
        }

        public void compute()
        {
            check();
            try
            {
                if (compute_chaine(mvm.Calcultext).Equals(""))
                {
                    mvm.num1 = "0";
                }
                else
                {
                    mvm.num1 = compute_chaine(mvm.Calcultext);

                }

                mvm.Historique.Add(mvm.Calcultext + " = " + mvm.num1);
                mvm.Calcultext = "Resultat = " + mvm.num1;
                
            }
            catch (Exception)
            {
                mvm.Calcultext = "Error!";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            addNumber("1");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            addNumber("2");
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            addNumber("3");
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            addNumber("4");
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            addNumber("5");
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            addNumber("6");
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            addNumber("7");
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            addNumber("8");
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            addNumber("9");
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            addNumber("0");
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!mvm.num1.Equals(""))
            {
                mvm.num1 = double.Parse(mvm.num1).ToString("#,###,###,###,##0.###");
            }

        }

        /*** === **/
        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            compute();
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            addOperator(" + ");

        }
        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            addOperator(" - ");
        }

        private void Button_Click_13(object sender, RoutedEventArgs e)
        {
            addOperator(" * ");
        }

        private void Button_Click_14(object sender, RoutedEventArgs e)
        {
            addOperator(" / ");
        }

        private void Button_Click_par(object sender, RoutedEventArgs e)
        {
            mvm.num1 = "";

            mvm.Calcultext += "(";

        }

        private void Button_par2(object sender, RoutedEventArgs e)
        {
            mvm.num1 = "";

            mvm.Calcultext += ")";
        }

        private void Button_del(object sender, RoutedEventArgs e)
        {
            if (mvm.Calcultext.Length > 0)
            {
                mvm.Calcultext = mvm.Calcultext.Substring(0, mvm.Calcultext.Length - 1);
                mvm.num1 = mvm.num1.Substring(0, mvm.num1.Length - 1);
            }

        }

        private void list_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
           // mvm.Calcultext = mvm.Historique.SelectedItem.ToString().Substring(0, list.SelectedItem.ToString().IndexOf(" = "));
          //  mvm.num1 = list.SelectedItem.ToString().Substring(list.SelectedItem.ToString().IndexOf("=") + 1, list.SelectedItem.ToString().Length - list.SelectedItem.ToString().IndexOf("=")-1);
        }

        private void Button_C(object sender, RoutedEventArgs e)
        {
            mvm.Calcultext = "";
            mvm.num1 = "";
        }

        public static string compute_chaine(string expression)
        {
            char[] chars = expression.ToCharArray();

            Stack<double> values = new Stack<double>();

            Stack<char> operation = new Stack<char>();

            for (int i = 0; i < chars.Length; i++)
            {
                if (chars[i] == ' ')
                {
                    continue;
                }

                if (chars[i] >= '0' && chars[i] <= '9')
                {
                    StringBuilder sbuild = new StringBuilder();
                    while (i < chars.Length && chars[i] >= '0' && chars[i] <= '9')
                    {
                        sbuild.Append(chars[i++]);
                    }
                    values.Push(double.Parse(sbuild.ToString()));
                }

                else if (chars[i] == '(')
                {
                    operation.Push(chars[i]);
                }

                else if (chars[i] == ')')
                {
                    while (operation.Peek() != '(')
                    {
                        values.Push(applyOp(operation.Pop(), values.Pop(), values.Pop()));
                    }
                    operation.Pop();
                }

                else if (chars[i] == '+' || chars[i] == '-' || chars[i] == '*' || chars[i] == '|' || chars[i] == '/' || chars[i] == '%' || chars[i] == '^')
                {

                    while (operation.Count > 0 && hasPrecedence(chars[i], operation.Peek()))
                    {
                        values.Push(applyOp(operation.Pop(), values.Pop(), values.Pop()));
                    }

                    operation.Push(chars[i]);
                }
            }

            while (operation.Count > 0)
            {
                try
                {
                    values.Push(applyOp(operation.Pop(), values.Pop(), values.Pop()));
                }
                catch (NotSupportedException e)
                {
                    return "Error!";
                }
                catch (Exception e)
                {
                    return "Error!";

                }
            }
            
                return values.Pop().ToString();

        }


        public static bool hasPrecedence(char op1, char op2)
        {
            if (op2 == '(' || op2 == ')')
            {
                return false;
            }
            if ((op1 == '*' || op1 == '/' || op1 == '%' || op1 == '^' || op1 == '|') && (op2 == '+' || op2 == '-'))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static double applyOp(char op, double b, double a)
        {
            switch (op)
            {
                case '+':
                    return a + b;
                case '-':
                    return a - b;
                case '*':
                    return a * b;
                case '%':
                    return a % b;
                case '^':
                    return Math.Pow(a, b);
                case '|':
                    if (b == 0)
                    {
                        throw new System.NotSupportedException("Error!");
                    }
                    return Convert.ToInt32(a / b);
                case '/':
                    if (b == 0)
                    {
                        throw new System.NotSupportedException("Error!");
                    }
                    return a / b;
            }
            return 0;
        }

        private void sizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.Height> this.Width)
            {
                this.grid.ColumnDefinitions[4].Width = new GridLength(0);
                this.grid.ColumnDefinitions[0].Width = new GridLength(Width / 4);
                this.grid.ColumnDefinitions[1].Width = new GridLength(Width / 4);
                this.grid.ColumnDefinitions[2].Width = new GridLength(Width / 4);
                this.grid.ColumnDefinitions[3].Width = new GridLength(Width / 4);
            }
            else
            {
                this.grid.ColumnDefinitions[4].Width = new GridLength(3* Width / 7);
                this.grid.ColumnDefinitions[0].Width = new GridLength(Width / 7);
                this.grid.ColumnDefinitions[1].Width = new GridLength(Width / 7);
                this.grid.ColumnDefinitions[2].Width = new GridLength(Width / 7);
                this.grid.ColumnDefinitions[3].Width = new GridLength(Width / 7);

            }
        }

        private void keyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine(e.Key);
             if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            {
                addNumber(e.Key.ToString().Substring(6));
            }
            else switch(e.Key)
                {
                    case Key.Add:
                        addOperator(" + ");
                        break;

                    case Key.Subtract:
                        addOperator(" - ");
                        break;

                    case Key.Multiply:
                        addOperator(" * ");
                        break;
                        
                    case Key.Divide:
                        addOperator(" / ");
                        break;
                    case Key.Oem3:
                        addOperator(" % ");
                        break;
                    case Key.Oem6:
                        addOperator(" ^ ");
                        break;
                    case Key.D6:
                        addOperator(" | ");
                        break;
                    case Key.Decimal:
                        mvm.Calcultext += ",";
                        mvm.num1 += ",";
                        break;
                    case Key.Delete:
                        if (mvm.Calcultext.Length >= 1)
                        {
                            mvm.Calcultext = mvm.Calcultext.Substring(0, mvm.Calcultext.Length - 1);
                            mvm.num1 = mvm.num1.Substring(0, mvm.num1.Length - 1);
                        }
                        break;
                    case Key.OemComma:
                        mvm.num1 = "";
                        mvm.Calcultext += ")";
                        break;
                    case Key.OemOpenBrackets:
                        if (mvm.Calcultext.Contains('('))
                        {
                            mvm.num1 = "";
                            mvm.Calcultext += " ) ";
                        }
                        break;
                    case Key.Enter:
                        compute();
                        break;

            }



        }

        private void Button_Vir(object sender, RoutedEventArgs e)
        {
            mvm.Calcultext += ",";
            mvm.num1 += ",";

        }

        private void Button_Click_pow(object sender, RoutedEventArgs e)
        {
            addOperator(" ^ ");

        }

        private void Button_Click_mod(object sender, RoutedEventArgs e)
        {
            addOperator(" % ");

        }

        private void Button_Click_div(object sender, RoutedEventArgs e)
        {
            addOperator(" | ");

        }
    }



}
